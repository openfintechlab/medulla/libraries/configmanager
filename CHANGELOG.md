# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.1.0] - 2020-07-07 (YYYY-MM-DD)

### Added
- Endpoint for loading service configuration
- Kubernetes specs for exposing cluster service

### Changed
N/A

### Removed
N/A


# Reference
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).