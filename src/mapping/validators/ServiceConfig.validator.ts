import Joi                              from "joi";
import  logger                          from "../../utils/Logger";
import {PostRespBusinessObjects}        from "../bussObj/Response";

export class ServiceConfigurationSchemaModel{
    ServiceConfiguration = Joi.object({
        "service_config":{
            "key": Joi.string().min(8).max(128).required(),
            "expiry": Joi.number().integer().min(0).max(432000).optional(),
            "values": Joi.object().pattern(/.*/, [Joi.string().max(128), Joi.number(), Joi.boolean()]).required()
        }
    });

     /**
     * Validate the provided JSON object and provide the parsed schema for CREATE function    
     * @param req (Object) JSON object containing valid ServiceconfigurationModel
     */
    public validateCreate(req:any): any{
        let {error,value} = this.ServiceConfiguration.validate(req);
        if(error){
            logger.error(`Validation error ${error}`);            
            let trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8154","Schema validation error",trace);                        
        }else{            
            return value;
        }
    }
}