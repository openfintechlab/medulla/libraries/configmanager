import RedisUtil    from "../utils/RedisUtil";
import AppConfig    from "../config/AppConfig";
import logger       from "../utils/Logger";
import {PostRespBusinessObjects}    from "../mapping/bussObj/Response";

class ConfigResponse{
    metadata: PostRespBusinessObjects.Metadata;
    config:Object;

    constructor(status: string, description:string, config:any){
        this.metadata = new PostRespBusinessObjects.Metadata();
        this.metadata.status = status;
        this.metadata.description = description;
        this.metadata.responseTime = new Date();
        this.config = config;
    }
}

export default class ConfigurationController{
    private serviceName!:string;
    public static redisUtil:RedisUtil;
    
    constructor(){
        
        ConfigurationController.redisUtil = new RedisUtil(AppConfig.redis_host,Number(AppConfig.redis_port), AppConfig.service_name);
    }

    /**
     * Instantiate the singleton object of connection manager
     * <b>NOTE!</b> This object uses AppConfig, configuration object to connect to REDIS
     */
    public static instance(){
        if(ConfigurationController.redisUtil === undefined){
            ConfigurationController.redisUtil = new RedisUtil(AppConfig.redis_host,Number(AppConfig.redis_port), AppConfig.service_name);
        }
    }


    static async  getConfigurations(serviceName:string){
        logger.info("Fetching service confguration for service: "+ serviceName);
        let serviceConfig = await ConfigurationController.redisUtil.get(serviceName);
        if(serviceConfig === undefined || serviceConfig === null){
            throw Error('Invalid Response');
        }
        let configResponse:ConfigResponse = new ConfigResponse("0000", "Success",JSON.parse(serviceConfig.toString()));
        return JSON.stringify(configResponse);
    }

    static async setConfiguration(serviceName:string, value:string){
        logger.info("Saving configuration in the cache for key:"+serviceName);
        await ConfigurationController.redisUtil.set(serviceName,value);        
    }

    static async deleteConfigurations(serviceName: string) {
        logger.info(`Deleting service configuration with key: ${serviceName}`);
        let result = await ConfigurationController.redisUtil.del(serviceName);
        return result;
    }

    static async isConnected(){
        let response = await ConfigurationController.redisUtil.ping();
        return response === 'PONG';
    }

    
}