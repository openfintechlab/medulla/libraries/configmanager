/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Wrapper Module for Generating Posting message generic response message
 * Please refer to project-docs/registiration/md-rb-001-resp.jsonc for mode details
 */
export namespace PostRespBusinessObjects {
    interface IGenericResponse {
        metadata: IMetadata;
    }

    interface IMetadata {
        status: String;
        description: String;
        responseTime: Date;
        trace: ITrace[];
    }

    interface ITrace {
        source: String;
        description: String;
    }

    /**
     * Wrapping all trace elements
     */
    export class Trace implements ITrace {
        source: String;
        description: String;

        constructor(source?: String, description?: String) {
            this.source = source || "";
            this.description = description || "";
        }
    }

    /**
     * Wrapper class for Metadata
     */
    export class Metadata implements IMetadata {
        status!: String;
        description!: String;
        responseTime!: Date;
        trace!: Trace[];
    }


    /**
     * Class for generating Posting Response
     */
    export class PostingResponse implements IGenericResponse {
        metadata: Metadata;

        constructor() {
            this.metadata = new Metadata();
        }

        /**
         * Generates a generic PostingResponse with status and description
         * @param status 4 characters status code
         * @param decsription  Description of the error message
         */
        public generateBasicResponse(status: String, decsription: String, ...trace: Trace[]): any {
            this.metadata.status = status;
            this.metadata.description = decsription;   
            this.metadata.responseTime = new Date();         
            if(trace.length > 0){
                this.metadata.trace = trace;
            }
            return this;
        }        

        get metadataRoot(): Metadata{
            return this.metadata;
        }

    }

}