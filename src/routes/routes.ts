/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express                              from "express";
import { ServiceConfigurationSchemaModel }  from "../mapping/validators/ServiceConfig.validator";
import util                                 from "util";
import {PostRespBusinessObjects}            from "../mapping/bussObj/Response";
import ConfigurationController              from "../mapping/ConfigurationController";
import logger                               from "../utils/Logger";



const router: any = express.Router();

/**
 * Configuration Manager endpoint for getting configuration
 * Endpoint should return the latest version automatically
 */
router.get("/config/:id", (req: any, res:any) => {
    // TODO! Fetch foniguration from the store
    res.set("Content-Type","application/json; charset=utf-8");
    if(req.params.id === undefined){
        res.status(404);   
        res.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9404","Invalid Request!"));        
    }else{
        // TODO! Load configuration from the database
        ConfigurationController.getConfigurations(req.params.id).then((resolve:any) => {
            logger.info("Response recevied from backend ");
            logger.debug(resolve);
            res.status(200);        
            res.send(JSON.parse(resolve));
        }).catch((error:any) => {
            logger.error("Error while fetching configuration: "+ error);  
            res.status(404);        
            res.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8404","Required Data Does Not Exist!"));        
        })
        
    }    
});

router.post("/config/", (req: any, res:any) => {
    res.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for adding configuration`);
    logger.debug(`Parsed response from validation: ${util.inspect(req.data,{compact:true,colors:true, depth: null})}`);
    // Step#1: Parse the request
    let parsedObj = new ServiceConfigurationSchemaModel().validateCreate(req.body);
    if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
        res.status(500);
        res.send(parsedObj)
    }else{
        // Step#2: Insert the configuration in cache server     
        try{
            let serviceName:string  = parsedObj.service_config.key;        
            let srvConfigBusObj: any = new Object();
            let values = new Array();
            values.push(Object.assign(parsedObj.service_config.values));
            srvConfigBusObj[serviceName] = values;
            logger.debug(`Storing Service Config Object: ${util.inspect(srvConfigBusObj,{compact:true,colors:true, depth: null})}`);
            ConfigurationController.setConfiguration(serviceName,JSON.stringify(srvConfigBusObj));
            // Step#3: Respond success in-case there is no exception
            res.status(201).send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!"));
        }catch(error){
            logger.debug(`Error occured while creating servie configuration  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
            res.status(500);
            if(error.metadata){            
                res.send(error);
            }else{            
                logger.debug(`Error: ${error}`);
                res.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
            }     
        }        
    }        
});

router.delete("/config/:id", async (req: any, res:any) => {
    res.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for deleting service configuration`);
    try{
        let result = await ConfigurationController.deleteConfigurations(req.params.id);
        logger.info(`Response after deleting: ${util.inspect(result,{compact:true,colors:true, depth: null})}`);        
        if(result == 0){
            res.status(404);        
            res.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8404","Required Data Does Not Exist!"));        
        }else{
            res.status(201).send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!"));
        }        
    }catch(error){
        logger.debug(`Error occured while creating servie configuration  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
            res.status(500);
            if(error.metadata){            
                res.send(error);
            }else{            
                logger.debug(`Error: ${error}`);
                res.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
            }     
    }
});



/**
 * Routes Definition for health, readiness and liveness check
 */
 // Route for liveness prone
 // The kubelet kills the container and restarts it.
router.get('/healthz',async (_: any,res: any)=> {
    // TODO! Check if number of configurations loaded
    res.set("Content-Type","application/json; charset=utf-8");
    let response:any;
    try{
        response = await ConfigurationController.isConnected();
        if(response){
            res.status(200);    
            res.send();
        }else{
            logger.error("Connection with backend broken");
            res.status(500);    
            res.send();
        }
    }catch(error){
        logger.error("Connection with backend broken");
        res.status(500);    
        res.send();
    }
        
});


// Route for rediness check. 
// This route will return 200 in-case all required bootstarap is finished
// Note: We want to suspend traffic in-case there is something wrong here
router.get('/readiness',(_: any,res: any)=> {
    // TODO! Check if the configuration is getting loaded or not
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);
    res.send();
});


// Protect slow starting containers with startup probes
router.get('/startup',(_: any,res: any)=> {
    // TODO! Check if the connectivity with backend is present or not
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);
    res.send();
});

export default router;