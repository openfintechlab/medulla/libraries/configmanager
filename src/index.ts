/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root entry level file forr bootstarting node js application
 */
import express      from 'express';
import chalk        from "chalk";
import helmet       from "helmet"
import morgan       from "morgan";

import ExpressApp   from "./utils/ExpressApp"
import router       from "./routes/routes";
import logger       from "./utils/Logger";
import AppConfig    from "./config/AppConfig";
import ConfigurationController from "./mapping/ConfigurationController";


// HEADER
logger.info(chalk.yellow("-----------------------------------"))
logger.info(chalk.bold.yellow("Medulla - The Financial Middleware"));
logger.info(chalk.bold.yellow("Copyright @ Openfintechlab.com"));
logger.info(chalk.yellow("-----------------------------------"))
logger.info(chalk.cyan("Starting Application"));

/**
 * Instantiating ExpressApp
 */
const app = new ExpressApp({
    contextRoot: AppConfig.contextRoot,
    port: AppConfig.port,    
    middlewares: [
        helmet(),
        express.json(),
        express.urlencoded({ extended: true }),
        morgan('combined')
    ],
    router: router    
});

// Connect with the Redis
async function connectWithRedis(){
    try{
        await ConfigurationController.instance();
        await ConfigurationController.redisUtil.connect();        
    }catch(error){
        logger.error("Error while connecting with Redis " + error);
        throw error;                
    }
}

connectWithRedis().then(() => {
    logger.info("Starting the listener");    
    app.listen();
    app.server.setTimeout(AppConfig.srv_timeout);
});