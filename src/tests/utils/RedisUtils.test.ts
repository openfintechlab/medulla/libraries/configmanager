import RedisUtils from "../../utils/RedisUtil";

const _VALUE_TO_INSERT:string   = "Value inserted by unit testing";
const _SERVICE_NAME:string      = "accounts";

describe('Redis Connectivity utility test', () => {    
    test('Connects to redis server SUCCESSFULLY', () => {
            const redisUtil:RedisUtils = new RedisUtils('baldeia',32010);            
            redisUtil.connect().then((resolve:string) => {
                expect(resolve).toBeDefined();                
                redisUtil.disConnect();
            }).catch((error:any) => {
                fail(error);
                redisUtil.disConnect();
            });
    });
    
    test('Connects to redis server UN-SUCCESSFULLY', () => {
        const redisUtil:RedisUtils = new RedisUtils('INVALID',32010);            
        redisUtil.connect().then((resolve:string) => {
            fail(resolve);            
        }).catch((error:any) => {
            expect(error).toBeDefined();
        }).finally(() => {
            redisUtil.disConnect();
        });
    });

    test('Get/Set a value in the REDIS db', ()=>{
        const redisUtil:RedisUtils = new RedisUtils('baldeia',32010);
        return redisUtil.connect().then((resolve:any) => {
            expect(resolve).toBe('Connected');
            return redisUtil.set(_SERVICE_NAME,_VALUE_TO_INSERT);            
        }).then((message:any) => {            
            expect(message).not.toBeDefined();            
            return redisUtil.get(_SERVICE_NAME)
        }).then((value:any) => {            
            expect(value).toBe(_VALUE_TO_INSERT);
        })
        .catch((error:any) => {
            fail(error);
        }).finally(() => {
            redisUtil.disConnect();
        });
    }); 
    
    test('Get value which does not exists', () => {
        const redisUtil:RedisUtils = new RedisUtils('baldeia',32010);
        return redisUtil.connect().then((resolve:any) => {
            expect(resolve).toBe('Connected');
            return redisUtil.del(_SERVICE_NAME)
        }).then((message:any) => {
            expect(message).toBeDefined();
            return redisUtil.get(_SERVICE_NAME)
        }).then((value:any) => {            
            expect(value).not.toBe(_VALUE_TO_INSERT);
        })
        .catch((error:any) => {
            fail(error);
        }).finally(() => {
            redisUtil.disConnect();
        });
    })

    test('Set value with expiry', () => {
        const redisUtil:RedisUtils = new RedisUtils('baldeia',32010);
        return redisUtil.connect().then((resolve:any) => {
            expect(resolve).toBe('Connected');
            return redisUtil.setex(_SERVICE_NAME,_VALUE_TO_INSERT, 10);            
        }).then((message:any) => {            
            expect(message).not.toBeDefined();            
            return redisUtil.get(_SERVICE_NAME)
        }).then((value:any) => {            
            expect(value).toBe(_VALUE_TO_INSERT);
        })
        .catch((error:any) => {
            fail(error);
        }).finally(() => {
            redisUtil.disConnect();
        });
    });

    test('Get/Set a value in the REDIS db with ASYNC/AWAT', async()=>{
        const redisUtil:RedisUtils = new RedisUtils('baldeia',32010);        
        await redisUtil.connect();
        await redisUtil.set(_SERVICE_NAME,_VALUE_TO_INSERT);
        let  value:any = await redisUtil.get(_SERVICE_NAME);
        expect(value).toBe(_VALUE_TO_INSERT);
        redisUtil.disConnect();
    }); 

    test('Get the base Tedis object and verify if it can exeute command', async() => {
        const redisUtil:RedisUtils = new RedisUtils('baldeia',32010,'accounts');        
        await redisUtil.connect();
        let output = await redisUtil.redisClient.command("PING");
        expect(output).toBeDefined();
        redisUtil.disConnect();
    });

    test('Connect to the server unsucessfully becase of invalid password', async() => {
        const redisUtil:RedisUtils = new RedisUtils('baldeia',32010,'test123');        
        try{
            let output = await redisUtil.connect();
        }catch(error){
            expect(error).toBeDefined();
        }                
    });
 
    
})