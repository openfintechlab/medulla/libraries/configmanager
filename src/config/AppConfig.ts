/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Module for loading configuration from .env file
 * Ref: https://github.com/motdotla/dotenv/tree/master/examples/typescript
 */


import logger from "../utils/Logger";
import * as dotenv from "dotenv";

const AppConfig  = {
    "env": process.env.NODE_ENV as string,
    "port": Number(process.env.OFL_MED_PORT || 3000) as number,
    "contextRoot": (process.env.OFL_MED_CONTEXT_ROOT ||  '/') as string,
    "srv_timeout": (process.env.OFL_MED_SRV_TIMEOUT || 10000 ) as number, /* Defaults to 10 seconds */
    "log_level": (process.env.OFL_MED_NODE_ENV || 'info' ) as string,
    "service_name" : (process.env.OFL_MED_SERVICENAME ) as string,
    "redis_host": (process.env.OFL_MED_REDIS_HOST) as string,
    "redis_port" : (process.env.OFL_MED_REDIS_PORT) as string,
    "redis_password" : (process.env.OFL_MED_REDIS_PASSWORD) as string
};

/**
 * Loads configuration from JSON to environment variable
 * @param jsonObj JSON object containing configuration 
 */
export function loadConfigfromJSONToEnv(jsonObj:any){    
    logger.debug("Setting Environment with config: "+ jsonObj);    
    let configCount:number = 0;
    for(var attrName in jsonObj){
        if(attrName !== "meta"){
            process.env[attrName] = jsonObj[attrName]; // Stores the configuration in current process 
            configCount++;                             // environment variable. This is not a good approach                                                        
        }                                              // as other contianer will be running in it's own user 
    }                                                  // space.
    if(configCount <= 0 ){
        logger.error("No configuration key recevied from CACHE.");
    }else{
        logger.info("Count of config keys serialize in environment = "+configCount);
    }
}

/**
 * 
 * @param envFilePath (Optional) Path to the location having .env
 */
export function loadConfigfromENVFile(envFilePath?:string){
    logger.info("Loading configuraiton from environment variable");
    if(envFilePath === undefined){
        dotenv.config({path: '${__dirname}'});
    }else{
        dotenv.config({path: envFilePath});
    }        
    
}


export default AppConfig;