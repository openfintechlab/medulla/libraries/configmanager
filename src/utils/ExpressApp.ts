/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Application definition file for express
 */

import express          from 'express';
import {Application}    from 'express';
import chalk            from "chalk";


import logger                       from "./Logger";
import AppConfig                    from "../config/AppConfig";
import {PostRespBusinessObjects}    from "../mapping/bussObj/Response";

export default class ExpressApp{
    private _ExpressApp: Application;
    private _PORT: number;
    private _SERVER: any;
    private _CONT_ROOT: string;

    /**
     * Instantiated ExpressApp application
     * @param options {port, middlewaresp[], routes[]}
     */
    constructor(options: {contextRoot: string; port: number; middlewares: any; router: any;}){
        this._ExpressApp    = express();
        this._PORT          = options.port;
        this._CONT_ROOT     = options.contextRoot;

        this.addMiddlewares(options.middlewares);        
        this.addRoutes(options.contextRoot,options.router); 
        

    }

    /**
     * Add middleware to the Express App
     * @param middleWares LIst of all middlewares
     */
    private addMiddlewares(middleWares: { forEach: (arg0: (middleWare: any) => void) => void; }) {
        middleWares.forEach(middleware => {
            this._ExpressApp.use(middleware);
        });
    }

    /**
     * Add routes
     * @param contextRoot Context Root of the Service Object. All other endpoints will be postfix to this route
     * @param router Express router objects having all routers instances
     */
    private addRoutes(contextRoot: string, router: any) {        
        this._ExpressApp.use(contextRoot, router);
    }   

    /**
     * Start the listener
     */
    public listen(){
        this._SERVER = this._ExpressApp.listen(this._PORT, () =>{
            logger.info(`Context root of the application: ` + chalk.underline.cyan.bold(`${this._CONT_ROOT}`));
            logger.info(`App listening on Port: ` +  chalk.underline.cyan.bold(`${this._PORT}`));
            logger.info(`App running in logging mode: `+chalk.underline.cyan.bold(`${AppConfig.log_level}`));
        });
    }

    private addGenericErrorMiddleware(){
        // Default route for displaying error message
        this._ExpressApp.use((req: any, res: any, _: any) => {
            let respObject: PostRespBusinessObjects.PostingResponse = 
                new PostRespBusinessObjects.PostingResponse();   
            logger.debug('Request:'+JSON.stringify(req.body));           
            res.set("Content-Type","application/json; charset=utf-8");                         
            res.status(406).send(respObject.generateBasicResponse("9404","Unable to process the request")); 
        });
    }    

    public get server(){
        return this._SERVER;
    }

    public get app(){
        return this._ExpressApp;
    }
    

}