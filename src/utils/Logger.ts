/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Logger utility class
 */
import log4js       from "log4js";
import AppConfig    from "../config/AppConfig"; 


const logger = log4js.getLogger();
logger.level = AppConfig.log_level;        
logger.debug('Logger initiated...');

export default logger;